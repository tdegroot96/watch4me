<?php

namespace Framework;

class Bootstrap {

    protected $_files = array();
    protected $_ignoredFiles = array();

    /**
     * Bootstrap constructor.
     * @param array $_ignoredFiles
     */
    public function __construct(array $_ignoredFiles) {
        $this->_ignoredFiles = $_ignoredFiles;
    }

    public function gatherFiles() {
        // TODO: System folder / Optional custom folders?
        $this->_files = $this->_getFilesFromDir(FRAMEWORK_DIR);
    }

    protected function _getFilesFromDir($dir) {
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        $files = array();

        /** @var SplFileInfo $fileInfo */
        foreach($iterator as $fileInfo) {
            if ($fileInfo->isFile()) {
                $fullPath = $fileInfo->getRealPath();
                if (in_array($fullPath, $this->_ignoredFiles) || $fileInfo->getExtension() !== 'php') {
                    continue;
                }
                var_dump($fileInfo->getExtension());
                $files[] = $fullPath;
            }
        }

        return $files;
    }

    public function fire() {
        foreach($this->_files as $file) {
            require_once($file);
        }
    }

}
