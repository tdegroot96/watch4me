<?php

namespace Framework;

use Exception;
use MysqliDb;
use Noodlehaus\Config;

class Application {

    /**
     * @var Application
     */
    private static $instance;

    /**
     * @var Config
     */
    protected $_config;

    /**
     * @var MysqliDb
     */
    protected $_database;

    /**
     * Application constructor.
     */
    private function __construct() {
        $this->_config = new Config(AR . '.env.json');
        $this->_database = new MysqliDb(
            $this->_config->get('database')
        );
    }

    /**
     * Initialize application
     * @throws Exception
     */
    public function init() {
        $this->_database->connect();
    }

    /**
     * @return Application
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new Application();
        }
        return self::$instance;
    }

    /**
     * @return Config
     */
    public function getConfig() {
        return $this->_config;
    }

    /**
     * @return MysqliDb
     */
    public function getDatabase() {
        return $this->_database;
    }

}