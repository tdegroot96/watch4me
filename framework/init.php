<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

function autoloader($classname) {
    $package = str_replace("\\", "/", $classname);
    $explode = explode('/', $package);
    $class = $explode[sizeof($explode) - 1];
    $package = implode('/', array_splice($explode, 0, sizeof($explode) - 1));
    $package = strtolower($package);
    require_once(AR . "$package/{$class}.php");
}

spl_autoload_register('autoloader');

Framework\Application::getInstance()
    ->init();