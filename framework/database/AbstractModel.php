<?php

namespace Framework\Database;

use Exception;
use Framework\Application;
use Framework\Entity;

class AbstractModel extends Entity {

    protected $_table = '';
    protected $_origData = array();

    /**
     * Load the model
     * @param $id
     * @throws Exception
     */
    public function load($id) {
        if ($this->_table === '') {
            throw new Exception('No table was set!');
        }

        if ($id === '') {
            throw new Exception('No id was given!');
        }

        $database = Application::getInstance()->getDatabase();

        if (!$database->tableExists($this->_table)) {
            throw new Exception("Table doesn't exist!");
        }

        $database->where('id', $id);
        $record = $database->getOne($this->_table);

        $this->setData($record);
        $this->setOrigData($record);

        return $this;
    }

    /**
     * Check if model is new or not
     * @return bool
     */
    public function isNew() {
        if ($id = $this->getData('id')) {
            $database = Application::getInstance()->getDatabase();
            $database->where('id', $this->getData('id'));
            return !$database->has($this->_table);
        }
        return true;
    }

    /**
     * Save the model
     * @return bool true on save action, false if nothing to save
     */
    public function save() {
        $diff = array_diff_assoc($this->getData(), $this->_origData);

        if (empty($diff)) {
            return false;
        }

        $database = Application::getInstance()->getDatabase();

        if ($this->isNew()) {
            $database->insert($this->_table, $this->getData());
        } else {
            $database->where('id', $this->getData('id'));
            $database->update($this->_table, $diff);
        }

        return true;
    }

    /**
     * Set original data
     * @param $data
     */
    public function setOrigData($data) {
        $this->_origData = $data;
    }

}