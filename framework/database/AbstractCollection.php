<?php

namespace Framework\Database;

use ArrayIterator;
use Countable;
use Exception;
use Framework\Application;
use Framework\Entity;
use IteratorAggregate;

class AbstractCollection extends Entity implements IteratorAggregate, Countable {

    /**
     * @var string
     */
    protected $_table = '';

    /**
     * @var AbstractModel
     */
    protected $_model = null;

    /**
     * @var array
     */
    protected $_items = array();

    /**
     * @var array
     */
    protected $_fields = array();

    /**
     * @var array
     */
    protected $_filters = array();

    /**
     * Load collection
     * @return $this
     * @throws Exception
     */
    public function load() {
        if ($this->_table === '') {
            throw new Exception('No table was set!');
        }

        $database = Application::getInstance()->getDatabase();

        if (!$database->tableExists($this->_table)) {
            throw new Exception("Table doesn't exist!");
        }

        $cols = $this->_fields;
        if (empty($cols)) {
            $cols = '*';
        }

        if (!empty($this->_filters)) {
            $firstFilter = $this->_filters[0];
            $database->where(
                $firstFilter['field'],
                $firstFilter['value'],
                $firstFilter['operator']
            );
            if (count($this->_filters) > 1) {
//                $i = 0;
//                foreach($this->_filters as $filter) {
//                    if ($i++ === 0) continue;
//                    $database->orWhere(
//
//                    )
//                }
            }
        }

        $data = $database->get($this->_table, null, $cols);
        $this->_loadModels($data);

        return $this;
    }

    /**
     * Load models into collection
     * @param $data
     * @return $this
     */
    protected function _loadModels($data) {
        foreach($data as $record) {
            /** @var AbstractModel $model */
            $model = new $this->_model();
            $model->setData($record);
            $model->setOrigData($record);

            $this->_items[] = $model;
        }
        return $this;
    }

    /**
     * @param $field
     * @return $this
     */
    public function addField($field) {
        if ($field === '*') {
            $this->_fields = array();
            return $this;
        }
        $this->_fields[] = $field;
        return $this;
    }

    public function addFilter($field, $value, $operator = '=') {
        $this->_filters[] = array(
            'field' => $field,
            'value' => $value,
            'operator' => $operator,
        );
    }

    public function getFirstItem() {
        $this->load();

        if (count($this->_items)) {
            reset($this->_items);
            return current($this->_items);
        }

        return new $this->_model();
    }

    /**
     * Retrieve an external iterator
     * @return ArrayIterator
     */
    public function getIterator() {
        $this->load();
        return new ArrayIterator($this->_items);
    }

    /**
     * Count elements in collection
     * @return int
     */
    public function count() {
        $this->load();
        return count($this->_items);
    }

}