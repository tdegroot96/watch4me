<?php

namespace Framework;

class Entity {

    /**
     * @var array
     */
    protected $_data = array();

    /**
     * Entity constructor.
     * @param array $_data
     */
    public function __construct(array $_data = array()) {
        $this->_data = $_data;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param bool $force Force value to be set even if it already exists
     * @return bool
     */
    public function setData($key, $value = '', $force = true) {
        if (is_array($key)) {
            $this->_data = $key;
        } else if (!isset($this->_data[$key]) || $force === true) {
            $this->_data[$key] = $value;
            return true;
        }
        return false;
    }

    /**
     * @param string|array $key
     * @return mixed
     */
    public function getData($key = '') {
        if (is_array($key)) {
            $result = array();
            foreach($key as $k => $v) {
                $result[$k] = $this->getData($k);
            }
            return $result;
        } else if ($key !== '') {
            if (isset($this->_data[$key])) {
                return $this->_data[$key];
            }
            return false;
        }
        return $this->_data;
    }

}