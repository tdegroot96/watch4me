<?php

use System\Model\Test\TestCollection;
use System\Model\Test\TestModel;

define('AR', dirname(__FILE__) . '/');

define('FRAMEWORK_DIR', AR . "/framework/");
define('SYSTEM_DIR', AR . "/system/");
define('WEB_DIR', AR . "/web/");
define('VENDOR_DIR', AR . "/vendor/");

require_once(VENDOR_DIR . 'autoload.php');
require_once(FRAMEWORK_DIR . 'init.php');
require_once(SYSTEM_DIR . 'router.php');