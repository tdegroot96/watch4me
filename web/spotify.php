<?php

use SpotifyWebAPI\SpotifyWebAPI;
use System\Model\Spotify\SpotifyAuthCollection;
use System\Model\Spotify\SpotifyAuthModel;

$collection = new SpotifyAuthCollection();
/** @var SpotifyAuthModel $authModel */
$authModel = $collection->getFirstItem();

$authModel->validate();

$spotifyApi = new SpotifyWebAPI();
$spotifyApi->setAccessToken($authModel->getData('access_token'));

var_dump($authModel->getTimeLeft());

$playlistLimit = 50;
$playlistOffset = 0;
$playlistOptions = array(
    'limit' => $playlistLimit,
    'offset' => $playlistOffset
);
?>
<ul>
    <?php while ($playlists = $spotifyApi->getMyPlaylists($playlistOptions)): ?>
        <?php foreach ($playlists->items as $item): ?>
            <li>
                <form method="post" action="/spotify/trackPlaylist">
                    <input type="hidden" name="playlist_id" value="<?= $item->id ?>">
                    <input type="hidden" name="owner_id" value="<?= $item->owner->id ?>">
                    <input type="submit" class="cursor-pointer" value="<?= htmlspecialchars($item->name) ?>">
                </form>
            </li>
        <?php endforeach; ?>
        <?php
        $playlistOffset = $playlistOffset + $playlistLimit;
        $playlistOptions = array(
            'limit' => $playlistLimit,
            'offset' => $playlistOffset
        );
        ?>
        <?php if (is_null($playlists->next)) break; ?>
    <?php endwhile; ?>
</ul>
<?php
//foreach ($playlists as $playlist) {
//    break;
//    $limit = 100;
//    $offset = 0;
//    $options = array(
//        'limit' => $limit,
//        'offset' => $offset
//    );
//    while ($tracks = $spotifyApi->getUserPlaylistTracks($playlist->owner->id, $playlist->id, $options)) {
//        $offset = $tracks->offset + $tracks->limit;
//        $options = array(
//            'limit' => $limit,
//            'offset' => $offset
//        );
//        var_dump(count($tracks->items));
//        if (is_null($tracks->next)) {
//            break;
//        }
//    }
//}
?>



