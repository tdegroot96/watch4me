<?php

$router = new AltoRouter();

$router->map('GET', '/', 'home::index', 'home');

$router->map('GET', '/spotify', 'spotify::index', 'spotify_home');
$router->map('POST', '/spotify/trackPlaylist', 'spotify::trackPlaylist', 'spotify/trackPlaylist');

$router->map('GET', '/test/index', 'test::index');

/** @var Closure $match */
$match = $router->match();

if ($match) {
    $target = $match['target'];

    $explode = explode('::', $target);
    if (sizeof($explode) <= 1) {
        echo "Invalid target: <strong>$target</strong>";
        die();
    }

    $controllerPrefix = "System\\Controller\\";
    $controller = $controllerPrefix . ucfirst($explode[0]) . 'Controller';

    $action = $explode[1];

    $controllerInstance = new $controller();
    call_user_func(array($controllerInstance, $action));
} else {
    header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
}
