<?php

namespace System\Controller;

use System\Model\Spotify\SpotifyPlaylistWatchModel;

class SpotifyController {

    public function index() {
        require_once(WEB_DIR . 'header.php');
        require_once(WEB_DIR . 'spotify.php');
        require_once(WEB_DIR . 'footer.php');
    }

    public function trackPlaylist() {
        $playlistId = $_POST['playlist_id'];
        $ownerId = $_POST['owner_id'];

        $watchModel = new SpotifyPlaylistWatchModel();
        $watchModel->setData('playlist_id', $playlistId);
        $watchModel->setData('owner_id', $ownerId);
        $watchModel->save();

        header('Location: /spotify');
        die();
    }

}