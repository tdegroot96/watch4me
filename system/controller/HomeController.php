<?php

namespace System\Controller;

class HomeController {

    public function index() {
        require_once(WEB_DIR . 'header.php');
        require_once(WEB_DIR . 'home.php');
        require_once(WEB_DIR . 'footer.php');
    }

}