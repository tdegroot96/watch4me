<?php

namespace System\Model\Test;

use Framework\Database\AbstractCollection;

class TestCollection extends AbstractCollection {

    protected $_table = 'test';
    protected $_model = TestModel::class;

}