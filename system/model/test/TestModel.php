<?php

namespace System\Model\Test;

use Framework\Database\AbstractModel;

class TestModel extends AbstractModel {

    protected $_table = 'test';

}