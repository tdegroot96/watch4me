<?php

namespace System\Model\Spotify;

use Framework\Application;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPIException;
use System\Model\Auth\AuthModel;

class SpotifyAuthModel extends AuthModel {

    protected $_table = 'spotify_auth';

    public function shouldRefresh() {
        return !($this->getTimeLeft() > 0);
    }

    public function getTimeLeft() {
        return ($this->getData('expires') * 1) - time();
    }

    public function validate() {
        $config = Application::getInstance()->getConfig();
        $session = new Session(
            $config->get('integration.spotify.clientId'),
            $config->get('integration.spotify.clientSecret'),
            $config->get('integration.spotify.endpoint')
        );

        if ($this->isNew()) {
            if (isset($_GET['code'])) {
                try {
                    $code = $_GET['code'];
                    $session->requestAccessToken($code);

                    $accessToken = $session->getAccessToken();
                    $refreshToken = $session->getRefreshToken();
                    $expires = $session->getTokenExpiration();

                    $this->setData(array(
                        'code' => $code,
                        'access_token' => $accessToken,
                        'token_type' => 'Bearer',
                        'expires' => $expires,
                        'refresh_token' => $refreshToken
                    ));

                    $this->save();
                } catch (SpotifyWebAPIException $e) {
                    echo '<pre>';
                    echo '<b>' . $e->getMessage() . '</b><br/>';
                    echo $e->getTraceAsString();
                    echo '</pre>';
                }

                header('Location: /spotify');
                die();
            } else {
                $scopes = $config->get('integration.spotify.scopes.scope');

                header('Location: ' . $session->getAuthorizeUrl($scopes));
                die();
            }
        } else if ($this->shouldRefresh()) {
            $session->refreshAccessToken($this->getData('refresh_token'));

            $accessToken = $session->getAccessToken();
            $expires = $session->getTokenExpiration();

            $this->setData('access_token', $accessToken);
            $this->setData('expires', $expires);

            $this->save();

            var_dump('Refreshed');
        }
    }
}