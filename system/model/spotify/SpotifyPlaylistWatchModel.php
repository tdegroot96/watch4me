<?php

namespace System\Model\Spotify;

use Framework\Database\AbstractModel;

class SpotifyPlaylistWatchModel extends AbstractModel {

    protected $_table = 'spotify_playlist_watch';

}