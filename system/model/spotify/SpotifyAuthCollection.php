<?php

namespace System\Model\Spotify;

use Framework\Database\AbstractCollection;

class SpotifyAuthCollection extends AbstractCollection {

    protected $_table = 'spotify_auth';
    protected $_model = SpotifyAuthModel::class;

}