<?php

namespace System\Model\Auth;

use Framework\Database\AbstractModel;

abstract class AuthModel extends AbstractModel {

    public abstract function validate();

}